#! /usr/bin/env python

import sys
import os
from datetime import datetime, timedelta
from time import sleep
import asyncio
import random

HERE = os.path.abspath(os.path.dirname(__file__))
ROOT = os.path.join(HERE, '..')

sys.path.append(ROOT)

from server.utils import drop_database  # noqa
from server.model.user import User  # noqa
from server.settings import config  # noqa
from server.model.notification import Notification  # noqa
from server.model.mentalcalculationhistory import Mentalcalculationhistory  # noqa
from server.model.photomemoryimage import Photomemoryimage  # noqa
from server.utils import DbSessionContext  # noqa

config.configure()
loop = asyncio.get_event_loop()
asyncio.set_event_loop(loop)

if config.get('env', 'production') != 'development':
    print('The "env" variable is not set to development')
    sys.exit(1)

DB_NAME = config.get('mongo_database_name')
drop_database(DB_NAME, config.get('redis_database'))

with DbSessionContext(DB_NAME) as session:
    # INSERT DUMMY DATA
    users = [
        {
            'name': 'test',
            'email': 'test@test.com',
            'email_validation_token': '123456',
            'password': '123456'
        }, {
            'name': 'to.disable',
            'email': 'to.disable@to.disable.com',
            'email_validation_token': '1337',
            'password': '123456'
        }, {
            'name': 'admin',
            'email': 'admin@admin.com',
            'password': '123456',
            'role': 'admin'
        }, {
            'name': 'disabled',
            'email': 'disabled@disabled.com',
            'password': '123456',
            'enable': False
        }
    ]

    for user_data in users:
        user = User()
        context = {
            'db_session': session,
            'method': 'create',
            'data': user_data
        }

        loop.run_until_complete(user.validate_and_save(context))
        if user.name == 'test':
            for index in range(40):
                notification = Notification()
                notification_data = {
                    'user_uid': user.get_uid(),
                    'message': 'Test {index}',
                    'target_url': '/profile',
                    'template_data': {
                        'index': str(index)
                    }
                }

                context = {
                    'db_session': session,
                    'method': 'create',
                    'data': notification_data
                }

                loop.run_until_complete(
                    notification.validate_and_save(context)
                )
                sleep(.1)

    user = session.query(User).filter(User.email == 'test@test.com').one()
    # MENTALCALCULATIONHISTORY
    for j in range(365):
        created_on = (
            datetime.now() - timedelta(days=j)
        )
        for i in range(50):
            operator = None

            if i < 10:
                operator = '+'
            elif i < 20:
                operator = '-'
            elif i < 30:
                operator = '/'
            else:
                operator = '*'

            mental_calculation_history = Mentalcalculationhistory()
            mental_calculation_history_data = {
                'answer': random.randint(1, 100),
                'number1': random.randint(1, 100),
                'number2': random.randint(1, 100),
                'result': random.choice([False, True]),
                'operator': operator,
                'created_on': created_on
            }
            context = {
                'db_session': session,
                'author': user,
                'method': 'create',
                'data': mental_calculation_history_data
            }

            loop.run_until_complete(
                mental_calculation_history.validate_and_save(context)
            )

    # Photo Memory Image
    # Dummy data for image urls.
    # url_list = [
    #     'https://upload.wikimedia.org/wikipedia/commons/4/4a/Cars_parked_on_the_pier_in_Barcelona_1997.jpg',  # noqa
    #     'https://upload.wikimedia.org/wikipedia/commons/2/20/Cars_in_queue_to_enter_Gibraltar_from_Spain.jpg',  # noqa
    #     'https://cdn.pixabay.com/photo/2015/03/26/10/31/cars-691497_960_720.jpg',  # noqa
    #     'https://cdn.pixabay.com/photo/2015/05/31/11/51/cars-791327_960_720.jpg',  # noqa
    #     'http://maxpixel.freegreatpicture.com/static/photo/1x/Toy-Cars-Childrens-Room-Autos-Vehicles-Model-Cars-1899757.jpg'  # noqa
    #     ]
    # for url in range(len(url_list)):
    #     image_path = (url_list[url])
    #     image = (url_list[url])

    #     photo_memory_image = Photomemoryimage() photo_memory_image_data = {
    #     'image_path': image_path } context = {     'db_session': session,
    #     'method': 'create',     'author': user,     'data':
    #     photo_memory_image_data }

    #     loop.run_until_complete(
    #         photo_memory_image.validate_and_save(context)
    #     )
