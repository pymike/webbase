### GENERAL

- [x] Ansible deployment
- [x] Not supported browser system
- [x] Cookie Banner
- [ ] Cookiecutter configuration
- [ ] EC2 deployment
- [ ] Os X deployment
- [ ] Documentation
- [ ] Amazon SES
- [ ] OpenID (twitter / facebook / google / github)

### TEST - CLIENT

- [ ] Profile
- [ ] Notification

### TEST - BROME

- [x] Auth flow
- [x] Login
- [x] Register
- [x] Translation
- [x] Privacy Policy
- [ ] Email confirmation
- [ ] Forgotten email
- [ ] Notification
- [ ] Not supported browser system
