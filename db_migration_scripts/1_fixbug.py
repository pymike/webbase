from server.model.migrationbase import MigrationBase
import datetime


class Migration(MigrationBase):
    def update(self, session):
        self.add_field('Mentalcalculationhistory', 'created_on', datetime.date.today)

    def roll_back(self, session):
        self.remove_field('Mentalcalculationhistory', 'created_on')
