from server.model.migrationbase import MigrationBase


class Migration(MigrationBase):
    def update(self, session):
        self.add_field('Mentalcalculationhistory', 'created_on', None)

    def roll_back(self, session):
        self.remove_field('Mentalcalculationhistory', 'created_on')
