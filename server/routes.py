from server.public.views import (
    index,
    api_get_session,
    api_validate_reset_password_token,
    api_send_reset_password_token
)
from server.auth.views import (
    api_admin,
    api_get_mental_calculation_stats,
    api_get_photo_memory_image,
    api_confirm_email,
    api_reset_password,
    Login,
    Register,
    Logout
)
from server.crud.views import CRUD

routes = [
    # CLIENT ROUTE => not /api/* and not /static/*
    ('GET', r'/{to:(?!api)(?!static).*}', index, 'index'),

    # API ROUTES
    ('POST', '/api/get_session', api_get_session, 'get_session'),
    ('GET', '/api/admin', api_admin, 'admin'),
    (
        'GET',
        '/api/get_mental_calculation_stats',
        api_get_mental_calculation_stats,
        'get_mental_calculation_stats'
    ),
    (
        'GET',
        '/api/get_photo_memory_image',
        api_get_photo_memory_image,
        'get_photo_memory_image'
    ),
    ('POST', '/api/confirm_email', api_confirm_email, 'api_confirm_email'),
    ('POST', '/api/reset_password', api_reset_password, 'api_reset_password'),
    (
        'POST',
        '/api/validate_reset_password_token',
        api_validate_reset_password_token,
        'api_validate_reset_password_token'
    ),
    (
        'POST',
        '/api/send_reset_password_token',
        api_send_reset_password_token,
        'api_send_reset_password_token'
    ),
    ('*', '/api/crud', CRUD, 'api_crud'),
    ('*', '/api/login', Login, 'api_login'),
    ('*', '/api/register', Register, 'api_register'),
    ('*', '/api/logout', Logout, 'api_logout'),
]
