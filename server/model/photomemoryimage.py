import os

from mongoalchemy.fields import (
    StringField
)
from werkzeug.datastructures import FileStorage

from server.model.basemodel import BaseModel
from server import exceptions
from server.settings import config


class Photomemoryimage(BaseModel):
    image_path = StringField()

    def __repr__(self):
        try:
            _repr = "Photomemoryimage <image_path: {self.image_path}>"  # noqa
            return _repr.format(
                self=self
                )
        except AttributeError:
            return "Photomemoryimage uninitialized"

##############################################################################
# FUNC
##############################################################################

    async def sanitize_data(self, context):
        author = context.get('author')
        data = context.get('data')

        if author:
            return data
        else:
            return []

    async def validate_and_save(self, context):
        data = context.get('data')
        db_session = context.get('db_session')

        # IMAGE PATH
        image_path = data.get('image_path')
        if image_path is not None:
            if type(image_path) == FileStorage:
                image_folder_path = config.get('image_folder_path')
                if not image_folder_path:
                    raise Exception(
                        'Please set the config "image_folder_path" in configs/server.json'  # noqa
                    )
                else:
                    image_obj = data.get('image_path')
                    image_path = os.path.join(
                        image_folder_path,
                        image_obj.filename
                    )
                    image_obj.save(image_path)

            self.image_path = image_path
        else:
            raise exceptions.MissingValueExceptions('image_path')

        # SAVE
        db_session.save(self, safe=True)

    async def method_autorized(self, context):
        author = context.get('author')
        method = context.get('method')

        if method in ['create', 'read']:
            if author:
                return True
            else:
                return False
        else:
            if author.role == 'admin':
                return True
            else:
                return False

    async def serialize(self, context):
        data = {}

        data['image_path'] = self.image_path

        return data
