from datetime import datetime, time

from mongoalchemy.document import Index
from mongoalchemy.fields import (
    ObjectIdField,
    BoolField,
    IntField,
    DateTimeField
)

from server.utils import SafeStringField
from server.model.basemodel import BaseModel
from server import exceptions


def set_to_midnight(_date):
    midnight = time(0)
    return datetime.combine(_date.date(), midnight)


class Mentalcalculationhistory(BaseModel):
    answer = SafeStringField()
    number1 = IntField()
    number2 = IntField()
    result = BoolField()
    operator = SafeStringField()
    created_on = DateTimeField(default=None)

    user_uid = ObjectIdField()

    # INDEX
    i_index = Index().ascending('user_uid')

    def __repr__(self):
        try:
            _repr = "Mentalcalculationhistory <answer: {self.answer}><number1: {self.number1}><number2: {self.number2}><operator: {self.operator}>"  # noqa
            return _repr.format(
                self=self
                )
        except AttributeError:
            return "Mentalcalculationhistory uninitialized"

##############################################################################
# FUNC
##############################################################################

    async def sanitize_data(self, context):
        author = context.get('author')
        method = context.get('method')
        data = context.get('data')

        if author:
            if author.role == 'admin':
                return data
            else:
                if method == 'create':
                    return data
                else:
                    editable_fields = []
        else:
            editable_fields = []

        return {k: data[k] for k in data if k in editable_fields}

    async def validate_and_save(self, context):
        data = context.get('data')
        author = context.get('author')
        db_session = context.get('db_session')

        # ANSWER
        answer = data.get('answer')
        if answer is not None:
            self.answer = answer
        else:
            raise exceptions.MissingValueExceptions('answer')

        # NUMBER1
        number1 = data.get('number1')
        if number1 is not None:
            self.number1 = number1
        else:
            raise exceptions.MissingValueExceptions('number1')

        # NUMBER2
        number2 = data.get('number2')
        if number2 is not None:
            self.number2 = number2
        else:
            raise exceptions.MissingValueExceptions('number2')

        # RESULT
        result = data.get('result')
        if result is not None:
            self.result = result
        else:
            raise exceptions.MissingValueExceptions('result')

        # OPERATOR
        operator = data.get('operator')
        if operator:
            self.operator = operator
        else:
            raise exceptions.MissingValueExceptions('operator')

        # CREATED ON
        created_on = data.get('created_on')
        if created_on:
            self.created_on = set_to_midnight(created_on)
        else:
            self.created_on = set_to_midnight(datetime.utcnow())

        # USER UID
        self.user_uid = author.get_uid()

        # SAVE
        db_session.save(self, safe=True)

    async def method_autorized(self, context):
        method = context.get('method')
        author = context.get('author')

        if author:
            if author.role == 'admin':
                return True
            else:
                if method in ['read', 'create']:
                    return True
                else:
                    return False
        else:
            return False

    async def serialize(self, context):
        data = {}

        data['answer'] = self.answer
        data['number1'] = self.number1
        data['number2'] = self.number2
        data['result'] = self.result
        data['operator'] = self.operator
        data['user_uid'] = str(self.get_uid())

        data['created_on'] = self.created_on.isoformat()

        return data
