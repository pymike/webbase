from mongoalchemy.document import Index
from mongoalchemy.fields import (
    ObjectIdField,
    IntField,
    BoolField
)

from server.model.basemodel import BaseModel

from dateutil.parser import parse
import pytz


class Mentalcalculationconfig(BaseModel):

    # ADDITION
    addition_bound_input_disabled = BoolField(default=True)
    addition_lower_bound_value = IntField(default=0)
    addition_upper_bound_value = IntField(default=100)

    # subtraction
    subtraction_bound_input_disabled = BoolField(default=True)
    subtraction_lower_bound_value = IntField(default=0)
    subtraction_upper_bound_value = IntField(default=100)

    # MULTIPLICATION
    multiplication_bound_input_disabled = BoolField(default=True)
    multiplication_lower_bound_value = IntField(default=0)
    multiplication_upper_bound_value = IntField(default=100)

    # DIVISION
    division_bound_input_disabled = BoolField(default=True)
    division_lower_bound_value = IntField(default=0)
    division_upper_bound_value = IntField(default=100)

    # INCREMENT
    increment_inputs_disabled = BoolField(default=True)
    increase_after_scores = IntField(default=10)
    increase_bounds_by = IntField(default=100)

    # DECREMENT
    decrement_inputs_disabled = BoolField(default=True)
    reduce_after_scores = IntField(default=10)
    reduce_bounds_by = IntField(default=100)

    user_uid = ObjectIdField()

    # INDEX
    i_index = Index().ascending('user_uid').unique()

    def __repr__(self):
        try:
            _repr = "Mentalcalculationconfig <user_uid: {self.user_uid}>"  # noqa
            return _repr.format(
                self=self
                )
        except AttributeError:
            return "Mentalcalculationconfig uninitialized"

##############################################################################
# FUNC
##############################################################################

    async def sanitize_data(self, context):
        author = context.get('author')
        data = context.get('data')

        if author:
            return data
        else:
            editable_fields = []

        return {k: data[k] for k in data if k in editable_fields}

    async def validate_and_save(self, context):
        data = context.get('data')
        author = context.get('author')
        db_session = context.get('db_session')

        config_query = db_session.query(Mentalcalculationconfig)\
            .filter(Mentalcalculationconfig.user_uid == author.get_uid())

        if config_query.count():
            self.mongo_id = config_query.one().get_uid()

        # ADDITION
        self.addition_bound_input_disabled = data.get(
            'additionBoundInputDisabled'
        )
        self.addition_lower_bound_value = data.get(
            'additionLowerBoundValue'
        )
        self.addition_upper_bound_value = data.get(
            'additionUpperBoundValue'
        )

        # subtraction
        self.subtraction_lower_bound_value = data.get(
            'subtractionLowerBoundValue'
        )
        self.subtraction_upper_bound_value = data.get(
            'subtractionUpperBoundValue'
        )
        self.subtraction_bound_input_disabled = data.get(
            'subtractionBoundInputDisabled'
        )

        # MULTIPLICATION
        self.multiplication_bound_input_disabled = data.get(
            'multiplicationBoundInputDisabled'
        )
        self.multiplication_lower_bound_value = data.get(
            'multiplicationLowerBoundValue'
        )
        self.multiplication_upper_bound_value = data.get(
            'multiplicationUpperBoundValue'
        )

        # DIVISION
        self.division_bound_input_disabled = data.get(
            'divisionBoundInputDisabled'
        )
        self.division_lower_bound_value = data.get(
            'divisionLowerBoundValue'
        )
        self.division_upper_bound_value = data.get(
            'divisionUpperBoundValue'
        )

        # INCREMENT
        self.increase_after_scores = data.get(
            'increaseAfterScores'
        )
        self.increase_bounds_by = data.get(
            'increaseBoundsBy'
        )
        self.increment_inputs_disabled = data.get(
            'incrementInputsDisabled'
        )

        # DECREMENT
        self.reduce_after_scores = data.get(
            'reduceAfterScores'
        )
        self.reduce_bounds_by = data.get(
            'reduceBoundsBy'
        )
        self.decrement_inputs_disabled = data.get(
            'decrementInputsDisabled'
        )

        self.user_uid = author.get_uid()

        db_session.save(self, safe=True)

    async def method_autorized(self, context):
        method = context.get('method')
        author = context.get('author')

        if method == 'create':
            if author:
                return True
            else:
                return False
        else:
            if author.get_uid() == str(self.user_uid):
                return True
            elif author.role == 'admin':
                return True
            else:
                return False

    async def serialize(self, context):
        data = {}

        # ADDITION
        data['additionBoundInputDisabled'] = self.addition_bound_input_disabled  # noqa
        data['additionLowerBoundValue'] = self.addition_lower_bound_value
        data['additionUpperBoundValue'] = self.addition_upper_bound_value

        # subtraction
        data['subtractionBoundInputDisabled'] = self.subtraction_bound_input_disabled  # noqa
        data['subtractionLowerBoundValue'] = self.subtraction_lower_bound_value  # noqa
        data['subtractionUpperBoundValue'] = self.subtraction_upper_bound_value  # noqa

        # MULTIPLICATION
        data['multiplicationBoundInputDisabled'] = self.multiplication_bound_input_disabled  # noqa
        data['multiplicationLowerBoundValue'] = self.multiplication_lower_bound_value  # noqa
        data['multiplicationUpperBoundValue'] = self.multiplication_upper_bound_value  # noqa

        # DIVISION
        data['divisionBoundInputDisabled'] = self.division_bound_input_disabled  # noqa
        data['divisionLowerBoundValue'] = self.division_lower_bound_value
        data['divisionUpperBoundValue'] = self.division_upper_bound_value

        # INCREMENT
        data['increaseAfterScores'] = self.increase_after_scores
        data['increaseBoundsBy'] = self.increase_bounds_by
        data['incrementInputsDisabled'] = self.increment_inputs_disabled

        # DECREMENT
        data['reduceAfterScores'] = self.reduce_after_scores
        data['reduceBoundsBy'] = self.reduce_bounds_by
        data['decrementInputsDisabled'] = self.decrement_inputs_disabled

        return data
