from mongoalchemy.document import Index
from mongoalchemy.fields import (
    EnumField,
    StringField,
    ObjectIdField
)

from server.model.basemodel import BaseModel
from server import exceptions


class Photomemoryquestion(BaseModel):
    question = StringField(max_length=2000)
    answer = StringField(max_length=2000)
    difficulty = EnumField(
        StringField(),
        'easy',
        'normal',
        'hard',
        default='normal'
    )
    question_type = EnumField(
        StringField(),
        'checkbox',
        'radiobutton',
        'singledropdown',
        'multipledropdown',
        'numberinput',
        'text',
        default='checkbox'
    )
    image_uid = ObjectIdField()

    # INDEX
    i_index = Index().ascending('image_uid')

    def __repr__(self):
        try:
            _repr = "Photomemoryquestion <image_uid: {self.image_uid}><question: {self.question}><answer: {self.answer}><difficulty: {self.difficulty}><question_type: {self.question_type}>"  # noqa
            return _repr.format(
                self=self
                )
        except AttributeError:
            return "Photomemoryquestion uninitialized"

##############################################################################
# FUNC
##############################################################################

    async def sanitize_data(self, context):
        author = context.get('author')
        method = context.get('method')
        data = context.get('data')

        if author:
            if author.role == 'admin':
                return data
            else:
                if method == 'create':
                    return data
                else:
                    editable_fields = [
                        'question'
                        'answer'
                        'difficulty'
                        'question_type'
                    ]
        else:
            editable_fields = []

        return {k: data[k] for k in data if k in editable_fields}

    async def validate_and_save(self, context):
        data = context.get('data')
        db_session = context.get('db_session')

        is_new = await self.is_new()

        # QUESTION
        question = data.get('question')
        if question is not None:
            self.question = question
        else:
            raise exceptions.MissingValueExceptions('question')

        # ANSWER
        answer = data.get('answer')
        if answer is not None:
            self.answer = answer
        else:
            raise exceptions.MissingValueExceptions('answer')

        # DIFFICULTY
        difficulty = data.get('difficulty')
        if difficulty is not None:
            self.difficulty = difficulty
        else:
            raise exceptions.MissingValueExceptions('difficulty')

        # QUESTION TYPE
        question_type = data.get('question_type')
        if question_type is not None:
            self.question_type = question_type
        else:
            raise exceptions.MissingValueExceptions('question_type')

        # IMAGE UID
        image_uid = data.get('image_uid')
        if image_uid is not None:
            self.image_uid = image_uid
        else:
            if is_new:
                raise exceptions.MissingValueExceptions('image_uid')

        db_session.save(self, safe=True)

    async def method_autorized(self, context):
        method = context.get('method')
        author = context.get('author')

        if method in ['create', 'read']:
            if author:
                return True
            else:
                return False
        else:
            if author.role == 'admin':
                return True
            else:
                return False

    async def serialize(self, context):
        data = {}

        data['question'] = self.question
        data['answer'] = self.answer
        data['difficulty'] = self.difficulty
        data['question_type'] = self.question_type
        data['image_uid'] = str(self.get_uid())

        return data
