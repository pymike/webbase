import os
from aiohttp_session import get_session
from aiohttp import web
from datetime import datetime, timedelta

from server import exceptions
from server.model.user import User
from server.model.emailconfirmationtoken import Emailconfirmationtoken
from server.model.mentalcalculationhistory import Mentalcalculationhistory
from server.model.photomemoryquestion import Photomemoryquestion
from server.model.photomemoryimage import Photomemoryimage
from server.model.resetpasswordtoken import Resetpasswordtoken
from server.settings import logger
from server.server_decorator import (
    require,
    exception_handler,
    csrf_protected
)
from server.auth import set_session, get_user_from_session
from server.utils import set_to_midnight


class Login(web.View):

    @exception_handler()
    @csrf_protected()
    async def post(self):
        try:
            data = await self.request.json()
            email = data['email']
            password = data['password']
        except:
            raise exceptions.InvalidRequestException('No json send')

        query = self.request.db_session.query(User)\
            .filter(User.email == email)
        if query.count():
            user = query.one()
            is_password_valid = await user.check_password(password)
            is_enable = user.enable
            if is_password_valid and is_enable:
                await set_session(user, self.request)
                session = await get_session(self.request)
                session['tz'] = data.get('user_timezone')

                context = {
                    'db_session': self.request.db_session,
                    'ws_session': session,
                    'method': 'read',
                    'queue': self.request.app.queue
                }

                resp_data = {
                    'success': True,
                    'token': session['csrf_token'],
                    'user': await user.serialize(context)
                }
            else:
                raise exceptions.WrongEmailOrPasswordException()
        else:
            raise exceptions.WrongEmailOrPasswordException(
                "Wrong email: '{email}'".format(email=email)
            )

        return web.json_response(resp_data)


class Register(web.View):

    @exception_handler()
    @csrf_protected()
    async def post(self):
        try:
            data = await self.request.json()
        except:
            raise exceptions.InvalidRequestException('No json send')

        context = {
            'db_session': self.request.db_session,
            'ws_session': {'tz': data.get('user_timezone')},
            'method': 'create',
            'queue': self.request.app.queue
        }

        # INIT USER
        user = User()
        context['data'] = data
        sane_data = await user.sanitize_data(context)
        context['data'] = sane_data
        await user.validate_and_save(context)

        # SET SESSION
        await set_session(user, self.request)
        session = await get_session(self.request)
        session['tz'] = data.get('user_timezone')

        context['method'] = 'read'
        context['user'] = user
        context['ws_session'] = session
        resp_data = {
            'success': True,
            'user': await user.serialize(context),
            'token': session['csrf_token']
        }
        return web.json_response(resp_data)


class Logout(web.View):

    @exception_handler()
    @require('login')
    @csrf_protected()
    async def post(self):
        session = await get_session(self.request)
        user = get_user_from_session(session, self.request.db_session)
        user.logout(session)
        resp_data = {'success': True}
        return web.json_response(resp_data)


@exception_handler()
@require('admin')
async def api_admin(request):
    logger.debug('admin')
    session = await get_session(request)
    user = get_user_from_session(session, request.db_session)

    context = {
        'user': user,
        'ws_session': session,
        'db_session': request.db_session,
        'method': 'read',
        'queue': request.app.queue
    }

    resp_data = {'success': True, 'user': await user.serialize(context)}
    return web.json_response(resp_data)


@exception_handler()
@require('login')
async def api_confirm_email(request):
    logger.debug('confirm_email')

    try:
        data = await request.json()
        email_confirmation_token = data['token']
    except:
        raise exceptions.InvalidRequestException('Missing json data')

    session = await get_session(request)
    user = get_user_from_session(session, request.db_session)

    context = {
        'user': user,
        'db_session': request.db_session,
        'ws_session': session,
        'method': 'update',
        'queue': request.app.queue
    }

    token_query = request.db_session.query(Emailconfirmationtoken)\
        .filter(Emailconfirmationtoken.token == email_confirmation_token)
    if token_query.count():
        email_confirmation_token = token_query.one()

        context['target'] = email_confirmation_token
        ret = email_confirmation_token.use(context)
        if ret:
            context['data'] = {'email_confirmed': True}
            del context['target']
            await user.validate_and_save(context)

            context['method'] = 'read'
            resp_data = {
                'success': True,
                'user': await user.serialize(context)
            }
            return web.json_response(resp_data)

    # TOKEN NOT FOUND
    else:
        raise exceptions.TokenInvalidException('token not found')


@exception_handler()
@csrf_protected()
@require('login')
async def api_reset_password(request):
    logger.debug('reset_password')

    try:
        data = await request.json()
        new_password = data['password']
        token = data['reset_password_token']
    except:
        raise exceptions.InvalidRequestException('Missing json data')

    session = await get_session(request)
    user = get_user_from_session(session, request.db_session)

    context = {
        'user': user,
        'db_session': request.db_session,
        'ws_session': session,
        'method': 'update',
        'queue': request.app.queue,
        'data': {'password': new_password}
    }

    token_query = request.db_session.query(Resetpasswordtoken)\
        .filter(Resetpasswordtoken.token == token)\
        .filter(Resetpasswordtoken.user_uid == user.get_uid())
    if token_query.count():
        reset_password_token = token_query.one()
        if reset_password_token.token == token:
            await user.validate_and_save(context)

            resp_data = {'success': True}
            return web.json_response(resp_data)

        else:
            raise exceptions.TokenInvalidException('Token mismatch')

    else:
        raise exceptions.TokenInvalidException('Token not found')


@exception_handler()
@require('login')
async def api_get_mental_calculation_stats(request):
    logger.debug('get_mental_calculation_stats')

    session = await get_session(request)
    db_session = request.db_session
    user = get_user_from_session(session, request.db_session)

    # STATS BY OPERATION

    addition_query = db_session.query(Mentalcalculationhistory)\
        .filter(Mentalcalculationhistory.operator == '+')\
        .filter(Mentalcalculationhistory.user_uid == user.get_uid())
    subtraction_query = db_session.query(Mentalcalculationhistory)\
        .filter(Mentalcalculationhistory.operator == '-')\
        .filter(Mentalcalculationhistory.user_uid == user.get_uid())
    multiplication_query = db_session.query(Mentalcalculationhistory)\
        .filter(Mentalcalculationhistory.operator == '*')\
        .filter(Mentalcalculationhistory.user_uid == user.get_uid())
    division_query = db_session.query(Mentalcalculationhistory)\
        .filter(Mentalcalculationhistory.operator == '/')\
        .filter(Mentalcalculationhistory.user_uid == user.get_uid())

    data = {
        'statsByOperation': {
            'addition': addition_query.count(),
            'subtraction': subtraction_query.count(),
            'multiplication': multiplication_query.count(),
            'division': division_query.count()
        }
    }

    # PERCENTAGE BY OPERATION

    # TODO make configurable by a post parameter
    since = 7

    OPERATORS = [
        {'sign': '+', 'name': 'addition'},
        {'sign': '-', 'name': 'subtraction'},
        {'sign': '*', 'name': 'multiplication'},
        {'sign': '/', 'name': 'division'},
    ]

    stats_by_percentage = []
    for i in range(since):
        date = set_to_midnight(datetime.today() - timedelta(days=i))
        stats_by_percentage.append(dict())
        stats_by_percentage[i]['date'] = date.strftime('%d/%m/%y')
        stats_by_percentage[i]['operatorsPercentage'] = dict()
        for operator in OPERATORS:
            correct = db_session.query(Mentalcalculationhistory)\
                .filter(Mentalcalculationhistory.user_uid == user.get_uid())\
                .filter(Mentalcalculationhistory.created_on == date)\
                .filter(Mentalcalculationhistory.operator == operator['sign'])\
                .filter(Mentalcalculationhistory.result == True).count()  # noqa

            incorrect = db_session.query(Mentalcalculationhistory)\
                .filter(Mentalcalculationhistory.user_uid == user.get_uid())\
                .filter(Mentalcalculationhistory.created_on == date)\
                .filter(Mentalcalculationhistory.operator == operator['sign'])\
                .filter(Mentalcalculationhistory.result == False).count()  # noqa

            key = '{name}_percentage'.format(name=operator['name'])
            total = correct + incorrect
            result = 0
            if total:
                result = round((correct / total) * 100, 2)

            stats_by_percentage[i]['operatorsPercentage'][key] = result

    stats_by_percentage.reverse()
    data['statsByPercentage'] = stats_by_percentage

    resp_data = {'success': True, 'results': data}
    return web.json_response(resp_data)

@exception_handler()
@require('login')
async def api_get_photo_memory_image(request):
    logger.debug('get_photo_memory_image')

    session = await get_session(request)
    db_session = request.db_session
    session = await get_session(request)

    read_context = {
        'db_session': request.db_session,
        'ws_session': session,
        'method': 'read',
        'queue': request.app.queue,
    }

    images_list = []

    images = db_session.query(Photomemoryimage).limit(20)
    # Loop over images query
    for index, image in enumerate(images):
        images_list.append(dict())
        images_list[index]['image'] = {}
        serialized_image = await image.serialize(read_context)

        # Query questions associated with the image
        associated_questions =  db_session.query(Photomemoryquestion)\
            .filter(Photomemoryquestion.image_uid == image.mongo_id)
        images_list[index]['questions'] = []

        for question in associated_questions:
            images_list[index]['questions'].append(await question.serialize(read_context))

        # Add Question and image path to same index on associated_image the list
        images_list[index]['image'] = os.path.basename(serialized_image['image_path'])
       
    resp_data = {'success': True, 'results': images_list}
    return web.json_response(resp_data)
