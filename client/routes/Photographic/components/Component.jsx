import React from 'react'
import { Tabs } from 'antd'
const TabPane = Tabs.TabPane

import Configuration from './Configuration'
import Play from './Play'
import Stats from './Stats'

// import { FormattedMessage } from 'react-intl'
// import ComponentStyle from './ComponentStyle.postcss'
import BaseComponent from 'core/BaseComponent'
import Loading from 'components/ux/Loading'
import ErrorMsg from 'components/ux/ErrorMsg'

class Photographic extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'onTabChange'
    )
    this.state = {
      activeTab: 0
    }
  }

  componentWillMount () {
    this.debug('componentWillMount')
    this.props.actions.doLoadImages()
  }
  // Change Tabs
  onTabChange (key) {
    this.debug(this.props.state.photographic.images)
    this.setState({activeTab: key})
  }

  render () {
    const {
      questionTimerValue,
      questionNoValue,
      imageTimerValue,
      images,
      error,
      loading
    } = this.props.state.photographic

    if (error) {
      return <ErrorMsg msgId={error} />
    } else if (loading) {
      return (
        <div className='container-fluid'>
          <Loading style={{left: '50%'}} />
        </div>
      )
    } else {
      return (
        <Tabs defaultActiveKey='1' onChange={this.onTabChange}>
          <TabPane tab='Configuration' key='1'>
            <Configuration />
          </TabPane>
          <TabPane tab='Play' key='2'>
          {(() => {
            if (this.state.activeTab === '2') {
              return (
                <Play
                  questionTimerValue={questionTimerValue}
                  questionNoValue={questionNoValue}
                  imageTimerValue={imageTimerValue}
                  images={images}
                />
              )
            } else {
              return null
            }
          })()}
          </TabPane>
          <TabPane tab='Statistic' key='3'>
            <Stats />
          </TabPane>
        </Tabs>
      )
    }
  }
}

module.exports = Photographic
