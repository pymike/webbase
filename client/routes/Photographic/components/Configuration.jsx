import React from 'react'
import './style.css'
import { Menu, Dropdown, Icon, Row, Col, Slider } from 'antd'
import 'antd/dist/antd.css'
import BaseComponent from 'core/BaseComponent'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from '../modules/reducer'

class Configuration extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'getConfiguration',
      'handleMenuClick',
      'handleVisibleChange',
      'onAfterChange'
    )

    const {
      imageTimerValue,
      questionTimerValue,
      questionNoValue
    } = this.props.state.photographic

    this.state = {
      visible: false,
      imageTimerValue: imageTimerValue,
      questionNoValue: questionNoValue,
      questionTimerValue: questionTimerValue
    }
  }

  handleMenuClick (e) {
    if (e.key === '3') {
      this.setState({ visible: true })
    }
  }

  handleVisibleChange (flag) {
    this.setState({ visible: flag })
  }

  /**
   ** Capture the name and the value of the slider
   ** Change the the state value of the slider
  */
  onAfterChange (value) {
    this.props.actions.doSetStateValue(value)
  }

  getConfiguration () {
    const {
      questionTimerValue,
      questionNoValue,
      imageTimerValue
    } = this.state

    const menu = (
      <Menu onClick={this.handleMenuClick}>i
        <Menu.Item key='1'>Easy</Menu.Item>
        <Menu.Item key='2'>Normal</Menu.Item>
        <Menu.Item key='3'>Hard</Menu.Item>
      </Menu>
    )

    return (
      <div>
        <Row>
          <Col span={20} push={4}>
            <Dropdown overlay={menu}
              oniVisibleChange={this.handleVisibleChange}
              visible={this.state.visible}
            >
              <a className='ant-dropdown-link' href='#'>
                Difficulty <Icon type='down' />
              </a>
            </Dropdown>
          </Col>
          <Col span={4} pull={20}>
            <p>Difficulty</p>
          </Col>
        </Row>
        <Row>
          <Col span={20} push={4}>
            <Slider
              defaultValue={imageTimerValue}
              onAfterChange={(value) => { this.onAfterChange({'imageTimerValue': value}) }}
            />
          </Col>
          <Col span={4} pull={20}>
            <p>Timer Image</p>
          </Col>
        </Row>
        <Row>
          <Col span={20} push={4}>
            <Slider
              defaultValue={questionTimerValue}
              onAfterChange={(value) => { this.onAfterChange({'questionTimerValue': value}) }}
            />
          </Col>
          <Col span={4} pull={20}>
            <p>Timer Question</p>
          </Col>
        </Row>
        <Row>
          <Col span={20} push={4}>
            <Slider
              defaultValue={questionNoValue}
              onAfterChange={(value) => { this.onAfterChange({'questionNoValue': value}) }}
            />
          </Col>
          <Col span={4} pull={20}>
            <p>No. of Question</p>
          </Col>
        </Row>
      </div>
      )
  }

  render () {
    return (
      <div>
        {this.getConfiguration()}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Configuration)

