import React from 'react'
// ant.design imports
import { Row } from 'antd'
import 'antd/dist/antd.css'

import BaseComponent from 'core/BaseComponent'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from '../modules/reducer'

// ant.design constants
const INDEX = 0
const serverpath = 'http://0.0.0.0:7777'

class Play extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'updateTimer',
      'getImage',
      'getQuestion',
      'reshuffleImages',
      'resetImageIndex',
      'getQuestions'
    )
    const {imageTimerValue, questionTimerValue, images} = this.props

    this.state = {
      imageIndex: INDEX,
      timer: imageTimerValue,
      interval: false,
      imageTimer: imageTimerValue,
      questionTimer: questionTimerValue,
      // radiobuttonValue: 1,
      // questions: images[INDEX]['questions']
      image: images[INDEX]['image']
    }
  }

  componentWillMount () {
    this.timerIntervalId = setInterval(this.updateTimer, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.timerIntervalId)
  }

  updateTimer () {
    if ((this.state.timer - 1) === 0 && !this.state.interval) {
      this.setState({
        interval: true,
        timer: this.state.questionTimer
      })
      this.resetImageIndex()
    }
    if ((this.state.timer - 1) === 0 && this.state.interval) {
      this.setState({
        interval: false,
        timer: this.state.imageTimer,
        imageIndex: this.state.imageIndex + 1
      })
      this.reshuffleImages()
    } else {
      this.setState({timer: (this.state.timer - 1)})
    }
  }

  reshuffleImages () {
    let {images} = this.props
    let {imageIndex} = this.state
    let image = images[imageIndex]['image']
    let questions = images[imageIndex]['image_questions']
    this.setState({image, questions})
  }

  resetImageIndex () {
    const {images} = this.props
    let {imageIndex} = this.state
    let imagesArrayLength = images.length
    if (imageIndex >= (imagesArrayLength - 1)) {
      this.setState({
        imageIndex: 0
      })
    }
  }

  getQuestion () {
    let {images} = this.props
    let {imageIndex} = this.state
    let questionsArray = images[imageIndex]['questions']
    return questionsArray.map((question, index) => {
      return (
        <li key={index}>
          {question.question}{question.question_type}
        </li>
      )
    })
  }

  // /**
  //  * Check the question type e.g checkbox, radio
  //  * button e.t.c to detetmine the answer html tag option
  //  * e.g {<input type="radio">}Yes {<input type="radio">}No
  //  * Read: http://www.w3schools.com/html/tryit.asp?filename=tryhtml_radio
  // */
  // getQuestionType (type) {
  //   let questionType = [
  //     {'type': 'checkbox'},
  //     {'type': 'radiobutton'},
  //     {'type': 'singledropdown'},
  //     {'type': 'multipledropdown'},
  //     {'type': 'numberinput'},
  //     {'type': 'text'}
  //   ]
  //   let type = questionType.find(type => type.type === type)
  // }

  // onradiobuttonChange(e) {
  //   console.log('radio checked', e.target.value);
  //   this.setState({
  //     radiobuttonValue: e.target.value,
  //   });
  // },

  // renderRadioButton (value, questionOptions) {
  //   return questionOptions.map((option, index) => {
  //     return (
  //       <RadioGroup onChange={this.onradiobuttonChange} value={this.state.value}>
  //         <ul>
  //           <li key={index}>
  //             <Radio value={index}>{option}</Radio>
  //           </li>
  //         </ul>
  //       </RadioGroup>
  //     )
  //   })
  // }

  getQuestions () {
    return (
      <div>
        <h3>Questions</h3>
        <hr />
        <ol type='1'>
          {this.getQuestion()}
        </ol>
      </div>
    )
  }

  /**
   * Render the images
   * @param: {serverpath} generate by  {python3 -m http.server 7777}
   * command
  */
  getImage () {
    let {image} = this.state
    let imagePath = `${serverpath}/${image}`
    return (
      <div>
        <Row>
          <img className='img-style center-block' src={imagePath} />
        </Row>
      </div>
    )
  }

  render () {
    let component
    if (this.state.interval) {
      component = this.getQuestions()
    }
    if (!this.state.interval) {
      component = this.getImage()
    }

    return (
      <div>
        <Row>
          <p>Timer: {this.state.timer}</p>
        </Row>
        {component}
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Play)
