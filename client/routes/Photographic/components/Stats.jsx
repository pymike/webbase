import React from 'react'
import BaseComponent from 'core/BaseComponent'

class Stats extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind()
    this.state = {}
  }

  render () {
    return (
      <div>
        <h1>Hello World</h1>
      </div>
    )
  }
}

module.exports = Stats
