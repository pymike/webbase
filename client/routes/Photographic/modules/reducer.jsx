import axios from 'axios'

// ====================================
// Constants
// ====================================

export const LOADING = 'LOADING'
export const LOADED_SUCCESS = 'LOADED_SUCCESS'
export const LOADED_ERROR = 'LOADED_ERROR'
export const SET_STATE_VALUE = 'SET_STATE_VALUE'
export const LOADED_URLS_SUCCESS = 'LOADED_URLS_SUCCESS'
export const FETCH_LOADING = 'FETCH_LOADING'
export const LOADED_STATS_SUCCESS = 'LOADED_STATS_SUCCESS'
export const LOADED_STATS_ERROR = 'LOADED_STATS_ERROR'

// ====================================
// Actions
// ====================================

const logger = require('loglevel').getLogger('Photographic')
logger.setLevel(__LOGLEVEL__)

export function doSetStateValue (stateObject) {
  return dispatch => {
    dispatch({
      type: SET_STATE_VALUE,
      stateObject
    })
  }
}

export function doLoadImages () {
  return dispatch => {
    axios.get('/api/get_photo_memory_image')
      .then((response) => {
        if (response.data.success) {
          dispatch({type: 'LOADED_STATS_SUCCESS', data: response.data})
        } else {
          dispatch({type: 'LOADED_STATS_ERROR', error: response.data.error})
        }
      })
  }
}

export const actions = {
  doSetStateValue,
  doLoadImages
}

// ====================================
// Reducers
// ====================================

const initialState = {
  visible: false,
  imageTimerValue: 10,
  questionTimerValue: 10,
  questionNoValue: 10,
  images: {},

  error: null,
  loading: false,
  data: null
}

export default function photographic (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return Object.assign({},
        initialState,
        {
          loading: true
        }
      )

    case SET_STATE_VALUE:
      let newStateObj = action.stateObject
      return Object.assign({},
        state,
        newStateObj
      )

    case FETCH_LOADING:
      return Object.assign({},
        state,
        {
          loading: true
        }
      )

    case LOADED_STATS_SUCCESS:
      return Object.assign({},
        state,
        {
          loading: false,
          images: action.data.results
        }
      )

    case LOADED_STATS_ERROR:
      return Object.assign({},
        state,
        {
          loading: false,
          error: action.error
        }
      )

    default:
      return state
  }
}
