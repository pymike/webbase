import axios from 'axios'

// ====================================
// Constants
// ====================================

export const CREATED_ERROR = 'CREATED_ERROR'
export const CREATED_CONFIG_ERROR = 'CREATED_CONFIG_ERROR'
export const LOADED_CONFIG_SUCCESS = 'LOADED_CONFIG_SUCCESS'
export const LOADED_CONFIG_ERROR = 'LOADED_CONFIG_ERROR'
export const FETCH_LOADING = 'FETCH_LOADING'
export const FETCH_SUCCESS = 'FETCH_SUCCESS'
export const FETCH_ERROR = 'FETCH_ERROR'
export const LOADING_STATS = 'LOADING_STATS'
export const LOADED_STATS_SUCCESS = 'LOADED_STATS_SUCCESS'
export const LOADED_STATS_ERROR = 'LOADED_STATS_ERROR'
export const SET_STATE_VALUE = 'SET_STATE_VALUE'

// ====================================
// Actions
// ====================================

const logger = require('loglevel').getLogger('MentalCalculation')
logger.setLevel(__LOGLEVEL__)

export function doLoadStats () {
  return dispatch => {
    axios.get('/api/get_mental_calculation_stats')
      .then((response) => {
        if (response.data.success) {
          dispatch({type: 'LOADED_STATS_SUCCESS', data: response.data})
        } else {
          dispatch({type: 'LOADED_STATS_ERROR', error: response.data.error})
        }
      })
  }
}
export function doSetStateValue (stateObject) {
  return dispatch => {
    dispatch({
      type: SET_STATE_VALUE,
      stateObject
    })
  }
}

export function doFetchConfig (data, cb = null) {
  return dispatch => {
    axios.post('/api/crud', data)
      .then((response) => {
        if (response.data.success) {
          dispatch({type: 'LOADED_CONFIG_SUCCESS', data: response.data.results[0]})
          if (cb) cb()
        } else {
          dispatch({type: 'LOADED_CONFIG_ERROR', error: response.data.error})
        }
      })
  }
}

export function doSaveConfig (token, mentalcalculation) {
  let data = {
    token,
    actions: {
      action: 'create',
      model: 'mentalcalculationconfig',
      data: {
        additionBoundInputDisabled: mentalcalculation.additionBoundInputDisabled,
        subtractionBoundInputDisabled: mentalcalculation.subtractionBoundInputDisabled,
        multiplicationBoundInputDisabled: mentalcalculation.multiplicationBoundInputDisabled,
        divisionBoundInputDisabled: mentalcalculation.divisionBoundInputDisabled,

        additionLowerBoundValue: mentalcalculation.additionLowerBoundValue,
        additionUpperBoundValue: mentalcalculation.additionUpperBoundValue,
        subtractionLowerBoundValue: mentalcalculation.subtractionLowerBoundValue,
        subtractionUpperBoundValue: mentalcalculation.subtractionUpperBoundValue,
        multiplicationLowerBoundValue: mentalcalculation.multiplicationLowerBoundValue,
        multiplicationUpperBoundValue: mentalcalculation.multiplicationUpperBoundValue,
        divisionLowerBoundValue: mentalcalculation.divisionLowerBoundValue,
        divisionUpperBoundValue: mentalcalculation.divisionUpperBoundValue,

        incrementInputsDisabled: mentalcalculation.incrementInputsDisabled,
        decrementInputsDisabled: mentalcalculation.decrementInputsDisabled,
        increaseAfterScores: mentalcalculation.increaseAfterScores,
        increaseBoundsBy: mentalcalculation.increaseBoundsBy,
        reduceAfterScores: mentalcalculation.reduceAfterScores,
        reduceBoundsBy: mentalcalculation.reduceBoundsBy
      }
    }
  }
  return dispatch => {
    axios.post('/api/crud', data)
      .then((response) => {
        if (!response.data.success) {
          dispatch({type: 'CREATED_CONFIG_ERROR', error: response.data.error})
        }
      })
  }
}

export function doFetch (data, cb = null) {
  return dispatch => {
    dispatch({type: 'FETCH_LOADING'})

    axios.post('/api/crud', data)
      .then((response) => {
        if (response.data.success) {
          dispatch({type: 'FETCH_SUCCESS', data: response.data.results})
          if (cb) cb()
        } else {
          dispatch({type: 'FETCH_ERROR', error: response.data.error})
        }
      })
  }
}

export function doCreate (data) {
  return dispatch => {
    axios.post('/api/crud', data)
      .then((response) => {
        if (!response.data.success) {
          dispatch(createdError(response.data.error))
        }
      })
  }
}

function createdError (error) {
  return {
    type: CREATED_ERROR,
    error
  }
}

export const actions = {
  doLoadStats,
  doFetchConfig,
  doCreate,
  doSaveConfig,
  doFetch,
  doSetStateValue
}

// ====================================
// Reducers
// ====================================

const initialState = {
  error: null,
  config: {},
  loading: false,
  formValid: false,
  // Addition
  additionBoundInputDisabled: false,
  additionLowerBoundValue: 1,
  additionUpperBoundValue: 100,
  // Subtraction
  subtractionBoundInputDisabled: true,
  subtractionLowerBoundValue: 1,
  subtractionUpperBoundValue: 100,
  // Multiplication
  multiplicationBoundInputDisabled: true,
  multiplicationLowerBoundValue: 1,
  multiplicationUpperBoundValue: 100,
  // Division
  divisionBoundInputDisabled: true,
  divisionLowerBoundValue: 1,
  divisionUpperBoundValue: 100,
  // Disable switches
  incrementInputsDisabled: true,
  decrementInputsDisabled: true,
  // Increment and decrement configurations
  increaseAfterScores: 10,
  increaseBoundsBy: 100,
  reduceAfterScores: 10,
  reduceBoundsBy: 100,

  // TIMER
  timerSliderValue: 10,
  timerSliderDisabled: true,

  timeout: 10000,
  history: [],
  stats: {}
}

function _isFormValid (config) {
  return !config.additionBoundInputDisabled || !config.subtractionBoundInputDisabled || !config.divisionBoundInputDisabled || !config.multiplicationBoundInputDisabled
}

export default function mentalcalculation (state = initialState, action) {
  switch (action.type) {

    case LOADING_STATS:
      return Object.assign({},
        state,
        {
          loading: true
        }
      )

    case SET_STATE_VALUE:
      let newStateObj = action.stateObject
      let newState = Object.assign({},
        state,
        newStateObj
      )
      let isFormValid = _isFormValid(newState)
      return Object.assign({},
        newState,
        {
          isFormValid
        }
      )

    case LOADED_STATS_SUCCESS:
      return Object.assign({},
        state,
        {
          stats: action.data.results,
          loading: false
        }
      )

    case LOADED_STATS_ERROR:
      return Object.assign({},
        state,
        {
          error: action.error,
          loading: false
        }
      )

    case LOADED_CONFIG_SUCCESS:
      return Object.assign({},
        state,
        {
          isFormValid: _isFormValid(action.data),
          ...action.data
        }
      )

    case LOADED_CONFIG_ERROR:
      return Object.assign({},
        state,
        {
          error: action.error
        }
      )

    case CREATED_ERROR:
      return Object.assign({},
        state,
        {
          error: action.error
        }
      )

    case FETCH_LOADING:
      return Object.assign({},
        state,
        {
          loading: true
        }
      )

    case FETCH_SUCCESS:
      return Object.assign({},
        state,
        {
          loading: false,
          history: action.data
        }
      )

    case FETCH_ERROR:
      return Object.assign({},
        state,
        {
          loading: false,
          error: action.error
        }
      )
    default:
      return state
  }
}
