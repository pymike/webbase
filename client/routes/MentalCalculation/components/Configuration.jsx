import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from '../modules/reducer'
import BaseComponent from 'core/BaseComponent'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'
import {
  Label,
  Container,
  Row,
  Col,
  FormGroup
} from 'reactstrap'

const Switch = global.Switch

export const OPERATORS = {
  addition: {
    'operator': '+',
    'lowerBoundName': 'additionLowerBoundValue',
    'upperBoundName': 'additionUpperBoundValue',
    'correctScore': 'additionCorrectScore'
  },
  subtraction: {
    'operator': '-',
    'lowerBoundName': 'subtractionLowerBoundValue',
    'upperBoundName': 'subtractionUpperBoundValue',
    'correctScore': 'subtractionCorrectScore'
  },
  multiplication: {
    'operator': '*',
    'lowerBoundName': 'multiplicationLowerBoundValue',
    'upperBoundName': 'multiplicationUpperBoundValue',
    'correctScore': 'multiplicationCorrectScore'
  },
  division: {
    'operator': '/',
    'lowerBoundName': 'divisionLowerBoundValue',
    'upperBoundName': 'divisionUpperBoundValue',
    'correctScore': 'divisionCorrectScore'
  }
}

class Configuration extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'getOperationConfig',
      'getIncrementConfig',
      'onCheckboxChange',
      'onAfterChangeState',
      'onAfterChange'
    )
    // this.state = {}
  }

  /**
   * Change the value of checkbox to true or false
   * @param {Object} state name and state value to
   * change the switch
  */
  onCheckboxChange (event) {
    let stateObject = {}
    let value = event.target.checked
    let name = event.target.dataset.valueName

    stateObject[name] = !value
    this.props.actions.doSetStateValue(stateObject)
  }

  /**
   * Update the value of slider
   * @param {Object} value is an array of two objects
   * The two objects are slider lowerbound and upperbound
   *
   * @param {Object} state name and state value to
   * change the slider
  */
  onAfterChangeState (value) {
    let newObject = Object.assign({}, value[0], value[1])
    this.props.actions.doSetStateValue(newObject)
  }

  onAfterChange (value) {
    this.props.actions.doSetStateValue(value)
  }

  /**
   * Timer slider
  */
  getTimerConfig () {
    const {
      timerSliderValue,
      timerSliderDisabled
    } = this.props.state.mentalcalculation

    return (
      <Container>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Timer'
                  checked={!timerSliderDisabled}
                  onChange={this.onCheckboxChange}
                  data-value-name='timerSliderDisabled'
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              tipTransitionName='rc-slider-tooltip-zoom-down'
              disabled={timerSliderDisabled}
              defaultValue={timerSliderValue}
              min={1}
              onAfterChange={(value) => {
                this.onAfterChange(
                  {'timerSliderValue': value}) }
              }
            />
          </Col>
        </Row>
      </Container>
    )
  }

  /**
   * Increment/decrement configuration sliders
  */
  getIncrementConfig () {
    const {
      incrementInputsDisabled,
      decrementInputsDisabled,
      increaseAfterScores,
      increaseBoundsBy,
      reduceAfterScores,
      reduceBoundsBy
    } = this.props.state.mentalcalculation

    return (
      <Container>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  checked={!incrementInputsDisabled}
                  data-value-name='incrementInputsDisabled'
                  label='Increase after X correct answer'
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              tipTransitionName='rc-slider-tooltip-zoom-down'
              disabled={incrementInputsDisabled}
              defaultValue={increaseAfterScores}
              min={1}
              onAfterChange={(value) => {
                this.onAfterChange(
                  {'increaseAfterScores': value}) }
              }
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <Label>{'   '}Increase by:</Label>
          </Col>
          <Col xs='8'>
            <Slider
              tipTransitionName='rc-slider-tooltip-zoom-down'
              disabled={incrementInputsDisabled}
              defaultValue={increaseBoundsBy}
              min={1}
              onAfterChange={(value) => {
                this.onAfterChange(
                  {'increaseBoundsBy': value}) }
              }
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Decrease after X wrong answer'
                  checked={!decrementInputsDisabled}
                  data-value-name='decrementInputsDisabled'
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              tipTransitionName='rc-slider-tooltip-zoom-down'
              disabled={decrementInputsDisabled}
              defaultValue={reduceAfterScores}
              min={1}
              onAfterChange={(value) => {
                this.onAfterChange(
                  {'reduceAfterScores': value}) }
              }
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <Label>{'   '}Decrease by:</Label>
          </Col>
          <Col xs='8'>
            <Slider
              tipTransitionName='rc-slider-tooltip-zoom-down'
              disabled={decrementInputsDisabled}
              defaultValue={reduceBoundsBy}
              min={1}
              onAfterChange={(value) => {
                this.onAfterChange(
                  {'reduceBoundsBy': value}) }
              }
            />
          </Col>
        </Row>
      </Container>
    )
  }

  /**
   * Upperbounds and lowerbounds configuration sliders
  */
  getOperationConfig () {
    const {
      additionLowerBoundValue,
      additionUpperBoundValue,
      additionBoundInputDisabled,
      subtractionLowerBoundValue,
      subtractionUpperBoundValue,
      subtractionBoundInputDisabled,
      multiplicationLowerBoundValue,
      multiplicationUpperBoundValue,
      multiplicationBoundInputDisabled,
      divisionLowerBoundValue,
      divisionUpperBoundValue,
      divisionBoundInputDisabled
    } = this.props.state.mentalcalculation

    return (
      <Container>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Addition'
                  data-value-name='additionBoundInputDisabled'
                  checked={!additionBoundInputDisabled}
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              range
              allowCross={false}
              min={1}
              max={500}
              onAfterChange={(value) => {
                this.onAfterChangeState(
                [{'additionLowerBoundValue': value[0]},
                {'additionUpperBoundValue': value[1]}]) }
              }
              defaultValue={[additionLowerBoundValue, additionUpperBoundValue]}
              disabled={additionBoundInputDisabled}
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Subtraction'
                  data-value-name='subtractionBoundInputDisabled'
                  checked={!subtractionBoundInputDisabled}
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              range
              allowCross={false}
              min={1}
              max={500}
              onAfterChange={(value) => {
                this.onAfterChangeState(
                [{'subtractionLowerBoundValue': value[0]},
                {'subtractionUpperBoundValue': value[1]}]) }
              }
              defaultValue={[subtractionLowerBoundValue, subtractionUpperBoundValue]}
              disabled={subtractionBoundInputDisabled}
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Multiplication'
                  data-value-name='multiplicationBoundInputDisabled'
                  checked={!multiplicationBoundInputDisabled}
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              range
              allowCross={false}
              min={1}
              max={500}
              onAfterChange={(value) => {
                this.onAfterChangeState(
                [{'multiplicationLowerBoundValue': value[0]},
                {'multiplicationUpperBoundValue': value[1]}]) }
              }
              defaultValue={[multiplicationLowerBoundValue, multiplicationUpperBoundValue]}
              disabled={multiplicationBoundInputDisabled}
            />
          </Col>
        </Row>
        <Row style={{marginBottom: 20}}>
          <Col xs='4'>
            <FormGroup check>
              <Label check>
                <Switch
                  label='Division'
                  data-value-name='divisionBoundInputDisabled'
                  checked={!divisionBoundInputDisabled}
                  onChange={this.onCheckboxChange}
                />
              </Label>
            </FormGroup>
          </Col>
          <Col xs='8'>
            <Slider
              range
              allowCross={false}
              min={1}
              max={500}
              onAfterChange={(value) => {
                this.onAfterChangeState(
                [{'divisionLowerBoundValue': value[0]},
                {'divisionUpperBoundValue': value[1]}]) }
              }
              defaultValue={[divisionLowerBoundValue, divisionUpperBoundValue]}
              disabled={divisionBoundInputDisabled}
            />
          </Col>
        </Row>
      </Container>
    )
  }

  render () {
    return (
      <div>
        <Container>
          <Row>
            <Col xs='6'>
              {this.getTimerConfig()}
            </Col>
          </Row>
          <Row>
            <Col xs='6'>
              {this.getOperationConfig()}
            </Col>
            <Col xs='6'>
              {this.getIncrementConfig()}
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Configuration)
