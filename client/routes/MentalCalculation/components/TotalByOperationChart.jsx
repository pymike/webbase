import React from 'react'
import { Pie } from 'react-chartjs-2'
import {
  Container,
  Row,
  Col
} from 'reactstrap'

import BaseComponent from 'core/BaseComponent'

class TotalByOperationChart extends BaseComponent {

  render () {
    const stats = this.props.stats
    const data = {
      labels: [
        'Addition',
        'Subtraction',
        'Multiplication',
        'Division'
      ],
      datasets: [{
        data: [
          stats['addition'],
          stats['subtraction'],
          stats['multiplication'],
          stats['division']
        ],
        backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
          '#FF5C60'
        ],
        hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
          '#FF5C60'
        ]
      }]
    }
    return (
      <Container>
        <Row>
          <Col xs={6}>
            <h3>Total number of result by operation type</h3>
            <Pie data={data} />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default TotalByOperationChart
