import React from 'react'
import { connect } from 'react-redux'
import { Container } from 'reactstrap'
import { Select } from 'antd'
import BaseComponent from 'core/BaseComponent'

const Option = Select.Option

function handleChange (value) {
  console.log(`selected ${value}`)
}

class StatsDropdown extends BaseComponent {

  render () {
    return (
      <Container style={{marginBottom: 20}}>
        <Select defaultValue='last_week'
          style={{ width: 900 }}
          showSearch={false}
          onChange={handleChange}
        >
          <Option value='today'>Today</Option>
          <Option value='last_week'>Last week</Option>
          <Option value='last_month'>Last month</Option>
          <Option value='last_year'>Last year</Option>
          <Option value='all_time'>All time</Option>
        </Select>
      </Container>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

export default connect(mapStateToProps)(StatsDropdown)
