import React from 'react'
import { Bar } from 'react-chartjs-2'
import {
  Container,
  Row,
  Col
} from 'reactstrap'

import BaseComponent from 'core/BaseComponent'

class ScoreByDateChart extends BaseComponent {
  render () {
    const {stats} = this.props
    let labels = []
    let additionData = []
    let subtractionData = []
    let multiplicationData = []
    let divisionData = []

    stats.map((stat, index) => {
      labels.push(stat['date'])
      additionData.push(stat['operatorsPercentage']['addition_percentage'])
      subtractionData.push(stat['operatorsPercentage']['subtraction_percentage'])
      multiplicationData.push(stat['operatorsPercentage']['multiplication_percentage'])
      divisionData.push(stat['operatorsPercentage']['division_percentage'])
    })
    const chartData = {
      labels,
      datasets: [{
        label: 'Addition',
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255, 99, 132, 1)',
        borderWidth: 1,
        data: additionData
      }, {
        label: 'Substration',
        backgroundColor: 'rgba(153, 102, 255, 0.2)',
        borderColor: 'rgba(153, 102, 255, 1)',
        borderWidth: 1,
        data: subtractionData
      }, {
        label: 'Division',
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        data: divisionData
      }, {
        label: 'Multiplication',
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        borderColor: 'rgba(54, 162, 235, 1)',
        borderWidth: 1,
        data: multiplicationData
      }]
    }
    const chartOptions = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            max: 100,
            callback: (label, index, labels) => {
              return label + '%'
            }
          }
        }],
        xAxes: [{}]
      }
    }
    return (
      <Container style={{marginTop: '2em'}}>
        <Row>
          <Col xs={12}>
            <h3>Score by operator type by date</h3>
            <Bar
              data={chartData}
              options={chartOptions}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default ScoreByDateChart
