import React from 'react'
import { connect } from 'react-redux'
import { Container } from 'reactstrap'
import BaseComponent from 'core/BaseComponent'
import ScoreByDateChart from './ScoreByDateChart'
import TotalByOperationChart from './TotalByOperationChart'
import StatsDropdown from './StatsDropdown'

class Stats extends BaseComponent {
  render () {
    return (
      <Container>
        <StatsDropdown />
        <TotalByOperationChart stats={this.props.state.mentalcalculation.stats['statsByOperation']} />
        <ScoreByDateChart stats={this.props.state.mentalcalculation.stats['statsByPercentage']} />
      </Container>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

export default connect(mapStateToProps)(Stats)
