import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from '../modules/reducer'
import {
  Table,
  Container,
  Row,
  Col
} from 'reactstrap'

import { OPERATORS } from './Configuration'
import ComponentStyle from './ComponentStyle.postcss'
import BaseComponent from 'core/BaseComponent'

let scoreObj = [
  {'operator': '+', 'correctScore': 'additionCorrectScore', 'inCorrectScore': 'additionWrongScores'},
  {'operator': '-', 'correctScore': 'subtractionCorrectScore', 'inCorrectScore': 'subtractionWrongScores'},
  {'operator': '/', 'correctScore': 'divisionCorrectScore', 'inCorrectScore': 'divisionWrongScores'},
  {'operator': '*', 'correctScore': 'multiplicationCorrectScore', 'inCorrectScore': 'multiplicationWrongScores'}
]

class Play extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'updateTimer',
      'appendResultToGameHistory',
      'getRandomInteger',
      'validateAnswer',
      'onAnswerInputKeyPressed',
      'genRandomGameState',
      'getRandomFactors',
      'getScores',
      'getRandomFactor'
    )

    const {
      history,
      increaseAfterScores,
      reduceBoundsBy,
      reduceAfterScores,
      timerSliderValue
    } = this.props.state.mentalcalculation

    this.state = {
      operators: this.getOperators(),
      timer: timerSliderValue,

      currentNumber1: null,
      currentNumber2: null,
      currentOperator: null,

      gameHistory: history,

      reduceBoundsBy: reduceBoundsBy,
      reduceAfterScores: reduceAfterScores,
      increaseAfterScores: increaseAfterScores,

      additionCorrectScore: 0,
      subtractionCorrectScore: 0,
      divisionCorrectScore: 0,
      multiplicationCorrectScore: 0,
      wrongScore: 0,

      initialLowerBound: 1,
      initialUpperBound: 10,

      timerSliderValue: timerSliderValue
    }
  }

  componentWillMount () {
    this.timerIntervalId = setInterval(this.updateTimer, 1000)
    this.genRandomGameState()
  }

  componentWillUnmount () {
    clearInterval(this.timerIntervalId)
  }

  updateTimer () {
    // TODO make the timer configurable
    if ((this.state.timer - 1) === 0) {
      this.setState({timer: this.state.timerSliderValue})
      this.genRandomGameState()
    } else {
      this.setState({timer: (this.state.timer - 1)})
    }
  }

  /**
   * @param: wrongScores, this.state{lower, upperbound}, initialLowerBound,
   *         initialUpperBound
   * @returns: initialLowerBound, initialUpperBound
   * Reset the bounds if the user gets a wrong answer
  */
  genRandomGameState () {
    let { operators } = this.state
    let { increaseAfterScores, increaseBoundsBy } = this.props.state.mentalcalculation
    let operator = operators[Math.floor(operators.length * Math.random())]
    let lowerBound = this.props[operator.lowerBoundName]
    let upperBound = this.props[operator.upperBoundName]
    let correctScore = operator.correctScore
    let lowerBoundName = operator.lowerBoundName
    let upperBoundName = operator.upperBoundName

    let newObject = {}
    if (this.state[correctScore] > 1 && this.state[correctScore] % increaseAfterScores === 0) {
      newObject[lowerBoundName] = lowerBound + increaseBoundsBy
      newObject[upperBoundName] = upperBound + increaseBoundsBy
      this.props.actions.doSetStateValue(newObject)
      this.setState({correctScore: 0})
      this.forceUpdate()
    }

    let minNumber = lowerBound
    let maxNumber = upperBound
    let number2 = this.getRandomInteger(minNumber, maxNumber)
    let number1
    if (operator.operator === '/' || operator.operator === '-') {
      number1 = this.getRandomFactors(number2)
    } else {
      number1 = this.getRandomInteger(minNumber, maxNumber)
    }

    // This will reset the bounds if user get a wrong answer.
    if (this.state.wrongScore) {
      let firstObject = {}
      let secondObject = {}
      firstObject[operator['lowerBoundName']] = this.state.initialLowerBound
      secondObject[operator['upperBoundName']] = this.state.initialUpperBound
      let newObject = Object.assign({}, firstObject, secondObject)
      this.props.actions.doSetStateValue(newObject)
      this.setState({wrongScores: 0})
      this.forceUpdate()
    }

    // CLEAR THE INPUT
    if (this.refs.answerInput) {
      this.refs.answerInput.value = ''
    }

    this.setState({
      currentNumber1: number2,
      currentNumber2: number1,
      currentOperator: operator.operator
    })
  }

  getRandomInteger (min, max) {
    return parseInt(Math.random() * (max - min) + min)
  }

  getRandomFactors (integer) {
    let factors = []
    let quotient = 0
    for (let i = 1; i <= integer; i++) {
      quotient = integer / i
      if (quotient === Math.floor(quotient)) {
        factors.push(i)
      }
    }
    this.getRandomFactor(factors)
  }

  getRandomFactor (factors) {
    let factor
    if (factors.length > 2) {
      factors.splice(0, 1)
      factors.splice(-1, 1)
      factor = factors[Math.floor(Math.random() * factors.length)]
    }
    if (factors.length > 1) {
      factors.splice(-1, 1)
      factor = factors[Math.floor(Math.random() * factors.length)]
    }
    factor = factors[Math.floor(Math.random() * factors.length)]
    return factor
  }

  getScores (result) {
    let newState = {}
    let { currentOperator } = this.state
    for (let value of scoreObj.keys()) {
      let operator = scoreObj[value]['operator']

      let correctScoreValue = 'this.state.' + scoreObj[value]['correctScore']
      let correctScore = scoreObj[value]['correctScore']

      if (operator === currentOperator && result) {
        // eslint-disable-next-line
        newState[correctScore] = eval(correctScoreValue) + 1
        this.setState(newState)
      }

      if (operator === currentOperator && !result) {
        this.setState({wrongScore: this.state.wrongScore ++})
      }
    }
  }

  onAnswerInputKeyPressed (event) {
    if (event.target.value !== '' && event.charCode === 13) {
      // submit
      let answer = event.target.value
      this.validateAnswer(parseInt(answer))
      this.genRandomGameState()
      this.setState({timer: this.state.timerSliderValue})
      this.refs['answerInput'].value = ''
    }
  }

  validateAnswer (answer) {
    let {
      currentNumber1,
      currentNumber2,
      currentOperator
    } = this.state

    let number1 = currentNumber1
    let number2 = currentNumber2
    let operator = currentOperator
    const whitespace = ' '

    // eslint-disable-next-line
    let realAnswer = parseInt(eval(number1 + whitespace + operator + whitespace + number2))
    let result
    if (realAnswer === answer) {
      result = true
      this.getScores(result)
      this.appendResultToGameHistory(result, number1, number2, operator, answer)
    } else {
      result = false
      this.appendResultToGameHistory(result, number1, number2, operator, answer)
      this.getScores(result)
    }
    let data = {
      token: this.props.state.session.token,
      actions: {
        action: 'create',
        model: 'mentalcalculationhistory',
        data: {
          answer: answer,
          number1: number1,
          number2: number2,
          operator: operator,
          result: result
        }
      }
    }
    this.props.actions.doCreate(data)
  }

  appendResultToGameHistory (result, number1, number2, operator, answer) {
    this.state.gameHistory.unshift({result, number1, number2, operator, answer})
    this.setState({gameHistory: this.state.gameHistory})
  }

  getOperators () {
    let operators = []
    let mentalcalculation = this.props.state.mentalcalculation
    if (!mentalcalculation.additionBoundInputDisabled) {
      operators.push(OPERATORS['addition'])
    }
    if (!mentalcalculation.subtractionBoundInputDisabled) {
      operators.push(OPERATORS['subtraction'])
    }
    if (!mentalcalculation.multiplicationBoundInputDisabled) {
      operators.push(OPERATORS['multiplication'])
    }
    if (!mentalcalculation.divisionBoundInputDisabled) {
      operators.push(OPERATORS['division'])
    }
    return operators
  }

  getHistoryDiv () {
    if (this.state.gameHistory.length) {
      return (
        <div style={{overflow: 'scroll', maxHeight: '300px'}}>
          <Table>
            <tbody>
              {(() => {
                return this.state.gameHistory.map((history, index) => {
                  let result = history.result ? 'right!' : 'wrong!'
                  return (
                    <tr key={index}>
                      <td>
                        {history.number1 + ' ' + history.operator + ' ' + history.number2 + ' = ' + history.answer}
                      </td>
                      <td>
                        {result}
                      </td>
                    </tr>
                  )
                })
              })()}
            </tbody>
          </Table>
        </div>
      )
    }
  }

  render () {
    return (
      <Container>
        <Row>
          <Col xs={4}>
            <h4>Timer: {this.state.timer}</h4>
          </Col>
        </Row>
        <Row>
          <Col xs={4}>
            <div className={ComponentStyle['play-number-div']}>
              {this.state.currentNumber1}
            </div>
          </Col>
          <Col xs={4}>
            <div className={ComponentStyle['play-operator-div']}>
              {this.state.currentOperator}
            </div>
          </Col>
          <Col xs={4}>
            <div className={ComponentStyle['play-number-div']}>
              {this.state.currentNumber2}
            </div>
          </Col>
        </Row>
        <Row style={{marginTop: 5}}>
          <Col>
            <input
              className='form-control'
              ref='answerInput'
              style={{textAlign: 'center'}}
              type='number'
              onKeyPress={this.onAnswerInputKeyPressed}
              placeholder='answer'
            />
          </Col>
        </Row>
        <Row style={{marginTop: 5}}>
          <Col>
            {this.getHistoryDiv()}
          </Col>
        </Row>
      </Container>
    )
  }
}

function mapStateToProps (state) {
  return {
    state
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Play)
