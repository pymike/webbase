import React from 'react'

import Play from './Play'
import Stats from './Stats'
import Configuration from './Configuration'
import BaseComponent from 'core/BaseComponent'
import Loading from 'components/ux/Loading'
import ErrorMsg from 'components/ux/ErrorMsg'

const Tabs = global.Tabs
const Tab = global.Tab
const TabList = global.TabList
const TabPanel = global.TabPanel

class MentalCalculation extends BaseComponent {
  constructor (props) {
    super(props)

    this._initLogger()
    this._bind(
      'fetchHistory',
      'onTabsChange'
     )
  }

  componentWillMount () {
    let data = {
      token: this.props.state.session.token,
      actions: {
        action: 'read',
        model: 'mentalcalculationconfig',
        filters: {
          user_uid: this.props.state.session.user.uid
        }
      }
    }
    this.props.actions.doFetchConfig(data)
    this.fetchHistory()
    this.props.actions.doLoadStats()
  }

  fetchHistory () {
    let data = {
      token: this.props.state.session.token,
      actions: {
        action: 'read',
        model: 'mentalcalculationhistory',
        limit: 20,
        descending: 'created_ts',
        filters: {
          user_uid: this.props.state.session.user.uid
        }
      }
    }
    this.props.actions.doFetch(data)
  }

  onTabsChange (selectedTabIndex, prevSelectedTabIndex) {
    if (prevSelectedTabIndex === 0) {
      this.props.actions.doSaveConfig(this.props.state.session.token, this.props.state.mentalcalculation)
    }
    if (prevSelectedTabIndex === 1 && selectedTabIndex === 2) {
      this.props.actions.doLoadStats()
    }
  }

  render () {
    let {
      additionLowerBoundValue,
      additionUpperBoundValue,
      subtractionLowerBoundValue,
      subtractionUpperBoundValue,
      multiplicationLowerBoundValue,
      multiplicationUpperBoundValue,
      divisionLowerBoundValue,
      divisionUpperBoundValue,
      error,
      loading,
      isFormValid
    } = this.props.state.mentalcalculation

    if (error) {
      return <ErrorMsg msgId={error} />
    } else if (loading) {
      return (
        <div className='container-fluid'>
          <Loading style={{left: '50%'}} />
        </div>
      )
    } else {
      return (
        <Tabs onChange={this.onTabsChange}>
          <TabList>
            <Tab>Configuration</Tab>
            <Tab isDisabled={!isFormValid}>Play!</Tab>
            <Tab>Statistic</Tab>
          </TabList>
          <TabPanel>
            <Configuration />
          </TabPanel>
          <TabPanel>
            <Play
              additionLowerBoundValue={additionLowerBoundValue}
              additionUpperBoundValue={additionUpperBoundValue}
              subtractionLowerBoundValue={subtractionLowerBoundValue}
              subtractionUpperBoundValue={subtractionUpperBoundValue}
              multiplicationLowerBoundValue={multiplicationLowerBoundValue}
              multiplicationUpperBoundValue={multiplicationUpperBoundValue}
              divisionLowerBoundValue={divisionLowerBoundValue}
              divisionUpperBoundValue={divisionUpperBoundValue}
            />
          </TabPanel>
          <TabPanel>
            <Stats stats={this.props.state.mentalcalculation.stats} />
          </TabPanel>
        </Tabs>
      )
    }
  }
}

module.exports = MentalCalculation
