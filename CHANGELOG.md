# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.2] - 2016-10-14
### Added
- Brome tests
- Cookie banner
- CRUD: filters_wildcard
- CRUD: uids
- Components examples
- Profile locale

## [0.0.1] - 2016-06-28
### Added
- initial release
